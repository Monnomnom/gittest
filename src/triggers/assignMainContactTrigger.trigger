trigger assignMainContactTrigger on Opportunity (after insert, after update) {

     if (Trigger.isAfter && Trigger.isInsert)
          {
              list<OpportunityContactRole> ocrList = new List <OpportunityContactRole>();
              
              for(Opportunity o : Trigger.new){

                    OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = o.Id, contactId = 
                                                                          [SELECT Id, Main_Contact__c
                                                                           FROM BusinessArea__c 
                                                                           WHERE Id =: o.BusinessArea__c
                                                                           LIMIT 1 ].Main_Contact__c
                                                                           , IsPrimary = true);
 
                	ocrList.add(ocr);

              }
              insert ocrList;
  		 }
    
    
  
    
}