trigger updateSalesCompanyTrigger on Account (after update) {

    List<Account> accToUpdate = new List<Account>();
    if(Trigger.isAfter && Trigger.isUpdate){   
        
        for(Account a: Trigger.Old){
           
            if(a.Type == 'Sales Company Data'){
   
            	Account parent = [SELECT Name,Id, Main_Company_Number__c,BillingStreet,
                     				 BillingCity, BillingState, BillingPostalCode,
                                     BillingCountry, BillingLatitude, BillingLongitude
                                  FROM Account
                                  WHERE Id =: [SELECT ParentId FROM Account WHERE Id=: a.Id].ParentId];   
                    
                
                if(parent.Name ==a.Name ){
                    Account newChild = Trigger.newMap.get(a.Id);
                    parent.Name = newChild.Name;
                    parent.BillingStreet = newChild.BillingStreet;
                    parent.BillingState = newChild.BillingState;
                    parent.BillingCity = newChild.BillingCity;
                    parent.BillingCountry = newChild.BillingCountry;
                    parent.BillingLatitude = newChild.BillingLatitude;
                    parent.BillingLongitude = newChild.BillingLongitude; 
                    accToUpdate.add(parent);
            	}
        
        	}
        }
        if(accToUpdate.size() > 0){
        	update accToUpdate; 
        }

       
      
    }
}