trigger reassignEventsTrigger on Holiday__c (after insert) {

     if (Trigger.isAfter && Trigger.isInsert){
         
         List<Event__c> eventsList = new List <Event__c>();
         for(Holiday__c h: Trigger.New){
           
            DateTime Start_Date = datetime.newInstance(h.Start_Date__c.year(), h.Start_Date__c.month(),h.Start_Date__c.day());
            DateTime End_Date = datetime.newInstance(h.End_Date__c.year(),h.End_Date__c.month(),h.End_Date__c.day()); 
             
            eventsList = [SELECT Id, Start_Date__c, End_Date__c 
                           FROM Event__c 
                           WHERE Start_Date__c >=:Start_Date
                           AND Start_Date__c <=: End_Date
                           AND 	CreatedById  =: h.CreatedById
                          ];
             
             List <Event__c> replacementEvents = [SELECT Id,Start_Date__c, End_Date__c
                                                  FROM Event__c
                                                  WHERE CreatedById =: h.Rep__c 
                                                  AND Start_Date__c >=:Start_Date
                                                  AND Start_Date__c <=: End_Date
                                                 ];
                          
    
            List<Event__c> reassignedEvents = new List<Event__c>();
             
            for(Event__c e : eventsList){ 
                 
                boolean freeDate = true; 
                 
                for(Event__c e2: replacementEvents){
                	
                     if((e2.Start_Date__c >= e.Start_Date__c && e2.Start_Date__c <= e.End_Date__c)||(e2.End_Date__c >= e.Start_Date__c  && e2.End_Date__c <=e.End_Date__c)){
                         freeDate = false; 
                         system.debug( e + ' happens during ' + e2 );
               		 }
           		 }
                 if(freeDate){
                      reassignedEvents.add(e);
                 }
                
                 freeDate = true;
                
                 
             }

             for(Event__c e :reassignedEvents){
                 
                 e.OwnerId = h.Rep__c;
                
             } 
             
            update reassignedEvents; 
             
         }
     }
}