trigger createSalesCompanyTrigger on Account (after insert, before insert) {

    List<Account> accToUpdate = new List<Account>();
    String duplicate = Label.companyNumberDuplicate;

    
   if(Trigger.isBefore && Trigger.isInsert){
        for(Account a: Trigger.New){
            if(a.Type =='Sales Company Data'){
            	
               try{
                    Account salesCompAccount2 = [SELECT Id, Company_Number__c, Main_Company_Number__c,Type
                                                  FROM Account
                                                  WHERE Company_Number__c =:a.Company_Number__c
                                                  AND Id !=:a.Id AND Main_Company_Number__c =: a.Main_Company_Number__c AND Type ='Sales Company Data' 
                                                  LIMIT 1];
                    a.addError(duplicate);    
                    }catch(Exception e){  }
            }
        }
   }
    
     if(Trigger.isAfter && Trigger.isInsert){
       
        for(Account a: Trigger.New){
          
            if(a.Type =='Sales Company Data'){

                Account salesCompAccount   = [SELECT Type,Id, Name, Main_Company_Number__c, Company_Number__c, ParentId, BillingStreet,
                     								 BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingLatitude, BillingLongitude
                                              FROM Account
                                              WHERE Id =:a.Id
                                              LIMIT 1];              
                
            	accToUpdate.add(salesCompAccount);
            }
        }
    }

    	if(accToUpdate.size() > 0){  
        updateSalesCompanyData.updateData(accToUpdate);
    	}
}