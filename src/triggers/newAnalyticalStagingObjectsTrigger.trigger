trigger newAnalyticalStagingObjectsTrigger on Analytical_Staging_Object__c (before insert) {
    
    if (Trigger.isBefore && Trigger.isInsert){
        
        List <Opportunity> oppList = new List<Opportunity>();
        List<Task> taskList = new List<Task>();
        List<Lead> leadList = new List<Lead>();
        
        //track the state of the ASOs
        List<Analytical_Staging_Object__c> updatedASO = new List<Analytical_Staging_Object__c> () ; 
        List<Analytical_Staging_Object__c> processedASO = new List <Analytical_Staging_Object__c> () ; 
        
        for(Analytical_Staging_Object__c aso : Trigger.New){
            
           /*  if(!Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}',aso.OppLeadOwnerEmail__c)){
          		system.debug('email format incorrect');
          		aso.OppLeadOwnerEmail__c = '';
   		 	}*/
   
            //create new opp
            if(aso.Type__c =='01'){
                
               Opportunity opp = new Opportunity(Name=aso.Name__c, StageName ='Prospecting' , 
                                                 CloseDate = Date.today() , //that part's only cuz of previous task
                                                 BusinessArea__c = [SELECT Id FROM BusinessArea__c WHERE Name ='Finance'].Id);
                
                try{
                opp.AccountId = [SELECT Id
                            FROM Account
                                 WHERE ExternalID__c=:aso.CustomerId__c ].Id; 
               
                }catch(Exception f){
                    system.debug('CustomerID doesn\'t consist of a proper ExternalID number ');
                }
                                                 
        	   try{ opp.OwnerId = [SELECT Id
                                   FROM User
                                   WHERE Email =: aso.OppLeadOwnerEmail__c
                                   LIMIT 1 ].Id ;
                   
                  } catch(Exception e){ 
                      
                      system.debug ('Assigned email was incorrect'); 
                      
                      try{
                      opp.OwnerId = [SELECT Id,OwnerId
                                     FROM Account
                                     WHERE Id =:opp.AccountId].OwnerId;
                     
                      } catch(Exception ee){
                          
                          system.debug('External ID must have been incorrect' );
                          opp.OwnerId = aso.OwnerId;
                      }
                  }
            
            oppList.add(opp);
            updatedASO.add(aso);    
                
            Task t = new Task(Type = 'AnalyticalOppTask', Priority='Normal', Subject ='Other', Status = 'In Progress', ActivityDate = Date.today().addDays(Integer.valueOf(aso.Number_of_Days__c))) ;  
            t.OwnerId = opp.OwnerId;
            taskList.add(t);
       
            }else
            
            //create new lead
            if(aso.Type__c == '02'){
                
                //optionally make a new field on the object and allow the user to pick the group they wish to assign the lead to, would probably require a visualforce page to make it
                //prettier
            	Lead l = new Lead(LastName = aso.Name__c, Company = aso.Name__c, Status= 'Open - Not Contracted', OwnerId = [SELECT Id FROM GROUP WHERE Name ='LeadQueue'].Id );
            	leadList.add(l);  
            	updatedASO.add(aso); 
                
            }else { 
                processedASO.add(aso);
            }
            
        }

        if(oppList.size() > 0){
            insert oppList;
        }
        
        if(taskList.size() > 0){
            insert taskList;
        }
        
        if(leadList.size() > 0){
            insert leadList;
        }
        
        if(updatedASO.size() > 0 ){
            
            for(Analytical_Staging_Object__c aso2: updatedASO){
                
                aso2.Status__c = 'OK';
            }
          
        }
        
        if(processedASO.size() > 0){
               for(Analytical_Staging_Object__c aso2: processedASO){
                
                aso2.Status__c = 'NOK';
            }
           
            
        }
    }
    
        processAnalyticalOppTasks.updateTasks();
 
}