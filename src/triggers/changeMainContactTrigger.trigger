trigger changeMainContactTrigger on BusinessArea__c (after Update) {

      if (Trigger.isAfter && Trigger.isUpdate){
          
          
          list<OpportunityContactRole> ocrList2 = new List <OpportunityContactRole>();
          
          for(BusinessArea__c ba: Trigger.new){
              
              if(ba.Main_Contact__c != trigger.old[0].Main_Contact__c)
              {
   					ocrList2 = [SELECT Id, ContactId FROM OpportunityContactRole WHERE ContactId  =: trigger.old[0].Main_Contact__c AND IsPrimary = true]; 
                    
                  for(OpportunityContactRole ocr : ocrList2){
                      
                      ocr.ContactId = ba.Main_Contact__c;
                  } 
               
              }
          }
        
          if(ocrList2.size() > 0){ 
        	 update ocrList2;
          }

    }
    
}