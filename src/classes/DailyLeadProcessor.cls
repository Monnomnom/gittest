global class DailyLeadProcessor implements Schedulable {

    global void execute(SchedulableContext ctx){
        List<Lead> leads  = [SELECT Id, LeadSource FROM Lead WHERE LeadSource = '' LIMIT 200];
       

        if(leads.size() > 0) {
            List<Lead> newLeads = new List<Lead>();
       		 for(Lead l : leads){
            
            l.LeadSource = 'Dreamforce';
            newLeads.add(l);
        }
        
       update newLeads; 
        
    	}
    }
}