public class processAnalyticalOppTasks {
    
    public static void updateTasks(){
        
        List<Task> taskList = [SELECT Id, Status, ActivityDate,OwnerId FROM Task WHERE Type = 'AnalyticalOppTask'];
        
        for(Task t: taskList){
              
            
            if(t.ActivityDate < Date.today().addDays(-14) && t.Status != 'Not Executed - Automatically closed' ){
                
                t.Status = 'Not Executed - Automatically closed';
                sendEmail(t);
            }
            
            t.IsReminderSet = true;
			t.ReminderDateTime = t.ActivityDate.addDays(-3);

        }
        update taskList;
    }
    
    
    private static void sendEmail(Task t){
        
        transient Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        email.setSubject( 'Task' + t.Id );
        List <String> emails = new List<String>();
        emails.add(String.valueOf([SELECT Email FROM User WHERE Id =: t.OwnerId].Email));
      	email.setToAddresses(emails);
      	email.setPlainTextBody('Your task' + t + ' has been automatically closed');
        
        if(!Test.isRunningTest()){
            	Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
    }
}