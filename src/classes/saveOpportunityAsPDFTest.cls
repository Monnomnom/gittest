@isTest
public class saveOpportunityAsPDFTest {
    
  @isTest static void noListItems(){
      
      Contact c = new Contact(FirstName= 'Finance', LastName = 'MainContact');
        insert c;
            
      BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = c.Id);
      insert ba;
      Opportunity opp = new Opportunity(Name ='TestOpportunity', StageName ='Prospecting', CloseDate = Date.today(), BusinessArea__c = ba.Id);
      upsert(opp);
      
      PageReference pageRef = new PageReference('https://c.eu6.visual.force.com/apex/saveOpportunityAsPDF?scontrolCaching=1&id='+opp.Id);
      ApexPages.StandardController sc = new ApexPages.standardController(opp);
      saveOppController sOC = new saveOppController(sc);
      Test.setCurrentPage(pageRef);
      
      boolean msgFound = false;
      for(Apexpages.Message msg : ApexPages.getMessages()){
        if (msg.getDetail() == sOC.msg_no_olt) msgFound = true;
        }
       system.assert(msgFound);
  }
    
    @isTest(seeAllData = true) static void yesListItems(){
        
      Contact c = new Contact(FirstName= 'Finance', LastName = 'MainContact');
        insert c;
            
        BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = c.Id);
        insert ba; 
        
      Account acc = new Account(Name ='Account1');
      upsert acc; 
        
      Pricebook2 pb = [SELECT Id, Name
                       	 FROM Pricebook2
                       		WHERE IsStandard = true limit 1];
      upsert pb; 
      
      Campaign cp = new Campaign(Name ='Campaign1');
      upsert cp;  
        
      Opportunity opp = new Opportunity(Name ='TestOpportunity', StageName ='Prospecting', CloseDate = Date.today(), AccountId = acc.Id, Pricebook2Id = pb.id, CampaignId =cp.Id, BusinessArea__c = ba.Id );
      upsert(opp);
        
      Product2 prod = New Product2(Name='prod1',ProductCode ='GS11111', IsActive = true);
      upsert prod; 
        
      PricebookEntry pe = new PricebookEntry(Pricebook2Id = pb.id, Product2Id = prod.Id , UnitPrice =100, UseStandardPrice = false, IsActive = true );
      upsert pe;
        
      OpportunityLineItem oli = new OpportunityLineItem(OpportunityId= opp.Id, PricebookEntryId = pe.Id, Quantity = 1 , TotalPrice = 200) ; 
      upsert oli;
          
      PageReference pageRef = new PageReference('https://c.eu6.visual.force.com/apex/saveOpportunityAsPDF?scontrolCaching=1&id='+opp.Id);
      ApexPages.StandardController sc = new ApexPages.standardController(opp);
      saveOppController sOC = new saveOppController(sc);
      Test.setCurrentPage(pageRef); 
        
      System.assertEquals(1,sOC.oppLineItem.size()); 
      
      System.assertEquals(cp.Name, sOC.campaign);
      System.assertEquals(pb.Name, sOC.pricebook);  
      System.assertEquals(acc.Name, sOC.accountName);    
    } 
}