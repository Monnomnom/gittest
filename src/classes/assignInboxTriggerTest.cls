@isTest
private class assignInboxTriggerTest {

    @isTest private static void createUser(){
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'UserProfile' LIMIT 1];
      	User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
     	EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      	LocaleSidKey='en_US', ProfileId = profileId.Id,TimeZoneSidKey='America/Los_Angeles', UserName='1111standarduser11@testorg.com');
        
        insert u;
        
        System.assert([SELECT Id from Project__c WHERE OwnerId =: u.Id].size()>0 );
        System.AssertEquals('Inbox',[SELECT Id,Name from Project__c WHERE OwnerId =: u.Id].Name);
        
        
    }
}