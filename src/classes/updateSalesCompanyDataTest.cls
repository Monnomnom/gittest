@isTest
private class updateSalesCompanyDataTest {
    
    
    @testSetup static void createAccounts(){
        Account mainAcc = new Account(Name='Main Account1', Type = 'Main Company Data', Main_Company_Number__c = 1);
        insert mainAcc; 
        
        Account childAcc = new Account(Name = 'Account 6', Type = 'Sales Company Data', Main_Company_Number__c = 2, Company_Number__c = 10, BillingStreet = 'Street',
                     				   BillingCity = 'City' , BillingState = 'State' , BillingPostalCode = '00000', BillingCountry = 'Country' );
        insert childAcc;
    }

    @isTest private static void insertedCorrectly(){
        List <Account> accList = new List <Account>();
        
        //correct accounts
        for(Integer i = 0; i<5 ;i++){
            Account a = new Account(Name = 'Account ' + i, Type = 'Sales Company Data', Main_Company_Number__c = 1, Company_Number__c = i);
            accList.add(a);
        }
        //accounts with no existing main company
        Account b = new Account(Name = 'Account 5', Type = 'Sales Company Data', Main_Company_Number__c = 3, Company_Number__c = 15);
        accList.add(b);
        
        //account with repeated company number but diff main company number
        Account c = new Account(Name = 'Account 6', Type = 'Sales Company Data', Main_Company_Number__c = 4, Company_Number__c = 10);
        accList.add(c);    
       
        insert accList;

        //was there a main ccompany account created? 
        System.AssertEquals('Account 5', [SELECT Name FROM Account WHERE Main_Company_Number__c = 3 AND Type = 'Main Company Data'].Name);
        
        //did account b have an assigned parent? 
        System.AssertEquals([SELECT ParentId
                             FROM Account
                             WHERE Company_Number__c = 15].ParentId,
                            [SELECT Id
                             FROM Account
                             WHERE Main_Company_Number__c = 3
                             AND Type = 'Main Company Data'].Id);
        
    }

    @isTest private static void duplicatedCompanyData(){
        String duplicate = Label.companyNumberDuplicate; 
        try{
        	Account c = new Account(Name = 'Account 6', Type = 'Sales Company Data', Main_Company_Number__c = 2, Company_Number__c = 10);
        	insert c;
    
        }catch(Exception e){ 
            
            System.Assert(e.getMessage().contains(duplicate));
        }
    }
    
    @isTest private static void updatedBilling(){
        
        
        Account c = [SELECT Type,Id, Name, Main_Company_Number__c, Company_Number__c, ParentId, BillingStreet,
                     		BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingLatitude, BillingLongitude
                     FROM Account
                     WHERE Type ='Sales Company Data'];
        
        c.BillingStreet = 'Other Street';
        c.Name = 'Other Name';
        update c;
        
        system.assertEquals(c.Name, [SELECT Name FROM Account WHERE Type = 'Main Company Data' AND Main_Company_Number__c =2 ].Name);
        system.assertEquals(c.BillingStreet, [SELECT BillingStreet FROM Account WHERE Type = 'Main Company Data' AND Main_Company_Number__c =2 ].BillingStreet);
        
    }


}