@isTest
public class TestVerifyDate {

    @isTest static void testDatesOrderError()
    {
       Date date1 = VerifyDate.CheckDates(Date.newInstance(2016,4,13), Date.today());
       System.assertEquals(Date.newInstance(2016, 4,30), date1);
    }
    @isTest static void testDatesWithin30Days()
    {
        Date date1 = VerifyDate.CheckDates(Date.today(),Date.today().addDays(20));
        System.assertEquals(Date.today().addDays(20),date1);
    }
    @isTest static void testDatesNotWithin30Days()
    {
        Date date1 = VerifyDate.CheckDates(Date.today(),Date.today().addDays(40));
        Date date2 = Date.newInstance(Date.today().year(), Date.today().month(),Date.daysInMonth(Date.today().year(),Date.today().month()));
        System.assertEquals(date2,date1);
    }
}