@isTest
public class AddPrimaryContactTest {

    	@testSetup
    static void setup(){
        
        List <Account>  accounts = new List<Account>();
        
        for(Integer i =0 ; i<50; i++){
             accounts.add(new Account(Name = 'Test Account' + i, BillingState ='CA' ));
            
        }
        for(Integer i = 50 ; i<100; i++){
             accounts.add(new Account(Name = 'Test Account' + i, BillingState ='NY' ));
            
        }
        insert accounts;
        system.debug('dasdsadsadsadsad');
    }
    
    static testmethod void testQueueable(){
        Contact c = new Contact (FirstName = 'Contact c ',LastName = 'Cont');
        insert c;
        
        AddPrimaryContact apc = new AddPrimaryContact(c,'CA');
        Test.startTest();
        System.enqueueJob(apc);
        Test.stopTest();
        System.assertEquals(50, [select count() from Contact where accountID IN (SELECT id FROM Account WHERE BillingState = 'CA')]);
        
    }
    
}