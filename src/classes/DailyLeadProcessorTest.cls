@isTest
private class DailyLeadProcessorTest {

    public static String cron_exp = '0 0 0 15 3 ? 2022';
    static testmethod void testScheduledJob(){
        List<Lead> leads = new List<Lead>();
        for(Integer i=0;i<200;i++){
            leads.add( new Lead(Company = 'C1', LastName='Last Name '+ i ));
            
        }
        insert leads;
        
        
        Test.startTest();
        String jobId =  System.schedule('DailyLeadProcessor', cron_exp, new DailyLeadProcessor());
      
        Test.stopTest();
       System.assertEquals(200, [SELECT Count() FROM Lead WHERE LeadSource = 'Dreamforce']);
    }
    
}