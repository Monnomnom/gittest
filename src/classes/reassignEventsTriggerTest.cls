@isTest
private class reassignEventsTriggerTest {

    
    
   /* @isTest private static void correctReassignment(){
        
        User u = [SELECT Id FROM User WHERE UserName ='1111standarduser11@testorg.com'];
        User u2 = [SELECT Id FROM User WHERE UserName ='1111standarduser12@testorg.com'];
        
        System.runAs(u){
            
        	Test.startTest();
        	Holiday__c h = new Holiday__c(Start_Date__c = Date.Today().addDays(1),End_Date__c = Date.Today().addDays(5), Rep__c = u2.Id );
            insert h;
            Test.stopTest();

            system.AssertEquals(u2.Id, [SELECT OwnerId FROM Event__c WHERE Name='Event11' LIMIT 1 ].OwnerId );
        }
        
    }*/
    
    @isTest private static void checkReassignment() {
        
        User u = [SELECT Id FROM User WHERE UserName ='1111standarduser11@testorg.com'];
        User u2 = [SELECT Id FROM User WHERE UserName ='1111standarduser12@testorg.com'];
        
        System.runAs(u){
            
            Test.startTest();
        	Holiday__c h = new Holiday__c(Start_Date__c = Date.Today().addDays(1),End_Date__c = Date.Today().addDays(5), Rep__c = u2.Id );
            insert h;
            Test.stopTest();
            
            
            //reassigned event
            system.AssertEquals(u2.Id, [SELECT OwnerId FROM Event__c WHERE Name='Event11' LIMIT 1 ].OwnerId );
            
            //no reassignemnt
            system.AssertEquals(u.Id, [SELECT OwnerId FROM Event__c WHERE Name='Event112' LIMIT 1 ].OwnerId );
            system.AssertEquals(u.Id, [SELECT OwnerId FROM Event__c WHERE Name='Event113' LIMIT 1 ].OwnerId );
        }
        
    }  
    
    @testSetup static void createEvents() {
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'UserProfile' LIMIT 1];
        
      	User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
     	EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      	LocaleSidKey='en_US', ProfileId = profileId.Id,TimeZoneSidKey='America/Los_Angeles', UserName='1111standarduser11@testorg.com');
        
      	User u2 = new User(Alias = 'standt2', Email='standarduser2@testorg.com',
      	EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US',
      	LocaleSidKey='en_US', ProfileId = profileId.Id,TimeZoneSidKey='America/Los_Angeles', UserName='1111standarduser12@testorg.com');
        
      	insert u;
        insert u2;  
        
        System.runAs(u) {
            
       		//event during the holiday     
      		Event__c event = New Event__c(Name ='Event11', Start_Date__c = DateTime.now().addDays(2), End_Date__c = DateTime.now().addDays(3));
        	insert event;
            
            //during the holiday but the reassigned person is busy
            Event__c event3 = New Event__c(Name ='Event113', Start_Date__c = DateTime.now().addDays(4), End_Date__c = DateTime.now().addDays(5));
        	insert event3;
            
       		// event after the holiday     
       		Event__c event2 = New Event__c(Name ='Event112', Start_Date__c = DateTime.now().addDays(20), End_Date__c = DateTime.now().addDays(21));
        	insert event2;      
        }
        
        System.runAs(u2){
            
            Event__c event = New Event__c(Name ='Event21', Start_Date__c = DateTime.now().addDays(4), End_Date__c = DateTime.now().addDays(5));
        	insert event;
        }
	}
}