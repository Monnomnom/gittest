@isTest
private class changeMainContactTriggerTest {

    	@isTest private static void checkMainContact() {
            
            BusinessArea__c ba =  [SELECT Id,Name, Main_Contact__c  FROM BusinessArea__c WHERE Name ='Finance'];
            Contact c2 = [SELECT Id FROM Contact WHERE LastName ='MainContact2'];
            ba.Main_Contact__c = c2.Id;
            
            Test.startTest();
            update ba;
            Test.stopTest();
            
            List<Opportunity> oppList = [SELECT Id FROM Opportunity];
            
            
   		
            for(Opportunity o: oppList){
                
                System.assertEquals(c2.Id,[SELECT Id, OpportunityId, ContactId FROM OpportunityContactRole WHERE OpportunityId =:o.Id AND IsPrimary = true ].ContactId );
            }

           
        
   		}
    
    
       @testSetup static void createSetup() {
        
        Contact c = new Contact(FirstName= 'Finance', LastName = 'MainContact');
        insert c;
           
        Contact c2 = new Contact(FirstName= 'Finance', LastName = 'MainContact2');
        insert c2;
                
        BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = c.Id);
        insert ba; 
    	
        List<Opportunity> oppList = new List<Opportunity>();
   		
        for(Integer i = 0; i<10;i++){
        
      		Opportunity o = new Opportunity (Name ='TestOpportunity' + i , StageName ='Prospecting', CloseDate = Date.today(), BusinessArea__c = ba.Id);
        	oppList.add(o);
   		 }
  
  	    if(oppList.size() >0 ){
   			Test.startTest();
       		insert oppList;
        	Test.stopTest();
   		 }
	
	} 
}