public class NewPricebooksController {
	public String selectedDivision {get; set;}  
	public String selectedPricebook {get; set;}     
	@testvisible private String MSG_CHOOSE_PRICEBOOK = 'Please choose a Pricebook';
	@testvisible private Opportunity opp {get; set;}
    
	public NewPricebooksController(ApexPages.StandardController stdController) {
		this.opp = (Opportunity)stdController.getRecord();   
   	}
     
    public List<SelectOption> getDivisions(){
        
        List<SelectOption> divisionsOptions = new List<SelectOption>();
        divisionsOptions.add(new SelectOption('','Please Select a Division')); 
        divisionsOptions.add(new SelectOption('01','01')); 
        divisionsOptions.add(new SelectOption('02','02')); 
        divisionsOptions.add(new SelectOption('03','03')); 
        divisionsOptions.add(new SelectOption('%','All'));
        return divisionsOptions;
    }
    
    public List<SelectOption> getPricebooks(){
        
        List<SelectOption> pricebooksOptions = new List<SelectOption>();
        pricebooksOptions.add(new SelectOption('','Please Select a Pricebook'));
        
        List<Pricebook2> pricebooksList = [SELECT Id,Name
                                           	 FROM Pricebook2
                                           		WHERE Name LIKE :selectedDivision+'%' ];
        
        for(Pricebook2 p : pricebooksList){
            pricebooksOptions.add(new SelectOption(p.Name,p.Name.right(p.Name.length()-3)));
        }
     
        return pricebooksOptions;
    }
    
    public PageReference updatePricebook2(){  
        
        try{
            opp.Pricebook2Id = [SELECT Id
                                	FROM Pricebook2
                                		WHERE Name =:selectedPricebook].id ;
            upsert(opp);
            
            opp.Pricebook2Id = [SELECT Id
                                	FROM Pricebook2
                                		WHERE Name =:selectedPricebook].id ;
            upsert(opp);
            
            PageReference pg = new PageReference('/'+opp.Id);
            return pg;
        } 
        	catch (Exception e) {
            if(selectedPricebook == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,MSG_CHOOSE_PRICEBOOK));
                 }
            return null;
            }
        
        } 
          
}