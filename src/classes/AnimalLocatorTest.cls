@isTest
public class AnimalLocatorTest {

    @isTest static void testGetCallout(){
        
        Integer animalId = 1;
        Test.setMock(HttpCalloutMock.class,new AnimalLocatorMock());
        String result = AnimalLocator.getAnimalNameById(animalId);
        System.assertNotEquals(null,result);
        System.assertEquals('chicken',result);

    }
}