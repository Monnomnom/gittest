@isTest private class newAnaylitcalStagingObjectsTriggerTest {

    @isTest private static void newOpp(){
        
        //correct opp 0
        Analytical_Staging_Object__c aso0 = new Analytical_Staging_Object__c(Type__c = '01',Name__c = 'Opportunity0',OppLeadOwnerEmail__c ='standarduser@testorg.com',
                                                                           Number_of_Days__c = 3,CustomerId__c = '000000' );
        
        //incorrect email opp 1
        Analytical_Staging_Object__c aso1 = new Analytical_Staging_Object__c(Type__c = '01',Name__c = 'Opportunity1',OppLeadOwnerEmail__c ='standarduser2@testorg',
                                                                           Number_of_Days__c = 3,CustomerId__c = '000000' );
        
        //incorrect email and customer ID opp 2
        Analytical_Staging_Object__c aso2 = new Analytical_Staging_Object__c(Type__c = '01',Name__c = 'Opportunity2',OppLeadOwnerEmail__c ='standarduser2@testorg',
                                                                           Number_of_Days__c = 3,CustomerId__c = '000001' );
        
        
        insert aso0;
        insert aso1;
        insert aso2;
        
        List<Opportunity> o = [SELECT Id, Name, OwnerId FROM Opportunity];
        List<Task> taskList = [SELECT Id, OwnerId, ActivityDate,Type FROM Task];
        
        system.Assert(o.size() > 0);
        system.Assert(taskList.size() > 0);
        
        for(Task t:taskList){
            
            system.AssertEquals('AnalyticalOppTask', t.Type);
            system.AssertEquals(Date.today().addDays(3), t.ActivityDate);
        }
        
        system.AssertEquals('Opportunity0', o[0].Name);
        system.AssertEquals([SELECT Id FROM User WHERE Email='standarduser@testorg.com'].Id, o[0].OwnerId);
        

        system.AssertEquals('Opportunity1', o[1].Name);
        system.AssertEquals([SELECT OwnerId FROM Account].OwnerId, o[1].OwnerId);
        
        system.Assert(o.size() > 0);
        system.AssertEquals('Opportunity2', o[2].Name);
        
    }
    @isTest private static void newLead(){

        Analytical_Staging_Object__c aso0 = new Analytical_Staging_Object__c(Type__c = '02',Name__c = 'Opportunity0',OppLeadOwnerEmail__c ='standarduser@testorg.com',
                                                                           Number_of_Days__c = 3,CustomerId__c = '000000' );
        
        insert aso0; 
        
        List<Lead> leadList = [SELECT Id ,LastName FROM Lead];
        
        System.assert(leadList.size() > 0);
        System.assertEquals('Opportunity0', leadList[0].LastName);
        
    }
    
    @isTest private static void updatedTasks(){
        
        //updated task (-20 days)
        Analytical_Staging_Object__c aso0 = new Analytical_Staging_Object__c(Type__c = '01',Name__c = 'Opportunity0',OppLeadOwnerEmail__c ='standarduser@testorg.com',
                                                                           Number_of_Days__c = -20,CustomerId__c = '000000' );
        
        //not updated
        Analytical_Staging_Object__c aso1 = new Analytical_Staging_Object__c(Type__c = '01',Name__c = 'Opportunity1',OppLeadOwnerEmail__c ='standarduser@testorg.com',
                                                                           Number_of_Days__c = 2,CustomerId__c = '000000' );
        
        insert aso0;
        insert aso1;
        
        List<Task> taskList = [SELECT Id,Status FROM Task];
        
        system.assertEquals('Not Executed - Automatically closed', taskList[0].Status);
        system.assert('Not Executed - Automatically closed' != taskList[1].Status);
        
    }
    
    @isTest private static void unknownType(){
        
         Analytical_Staging_Object__c aso0 = new Analytical_Staging_Object__c(Type__c = '03',Name__c = 'Opportunity0',OppLeadOwnerEmail__c ='standarduser@testorg.com',
                                                                           Number_of_Days__c = -20,CustomerId__c = '000000' );
        
         insert aso0;
        
         system.assertEquals('NOk', [SELECT Status__c FROM Analytical_Staging_Object__c].Status__c);
        
    }
    @testSetup static void prepareForTests(){
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'UserProfile' LIMIT 1];
        
      	User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
     	EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      	LocaleSidKey='en_US', ProfileId = profileId.Id,TimeZoneSidKey='America/Los_Angeles', UserName='1111standarduser11@testorg.com');
        insert u;
              
        Contact c = new Contact(FirstName= 'Finance', LastName = 'MainContact');
        insert c;
            
        BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = c.Id);
        insert ba;
        
        Account a = new Account(Name = 'TestAccount',ExternalID__c= '000000');
        insert a;

    }

}