public with sharing class CampingListController {
  @AuraEnabled
    public static List<Camping_Item__c> getItems() {
       return [SELECT Id, Name, Price__c, Quantity__c, Packed__c FROM Camping_Item__c];
   }
   
    @AuraEnabled
   public static Camping_Item__c saveItem(Camping_Item__c item) {
     upsert item;
      return item;
 }
    
    
    /*
    @AuraEnabled
    public static List<Camping_Item__c> getItems(){
        
        String[] fieldsToCheck = new String[]{
            'Id','Name','Packed__c','Price__c','Quantity__c'
        };
            Map<String, Schema.SObjectField> fieldDEscribeTokens = Schema.SObjectType.Camping_Item__c.fields.getMap();
        for(String field: fieldsToCheck){
            if(!FieldDescribeTokens.get(field).getDescribe().isAccessible()){
                throw new System.NoAccessException();return null;
            }
        }
        
        return [SELECT Id, Name, Quantity__c, Price__c,Packed__c FROM Camping_Item__c ];
    }
    
    @AuraEnabled
    public static Camping_Item__c saveItem (Camping_Item__c CampingItem){
        upsert CampingItem;
        return CampingItem;
    }
    
    */
}