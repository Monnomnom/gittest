global class AccountProcessor {

    
    @future
    public static void countContacts(Set<Id> accountIds){
        
        List<Account> accounts = [Select Id,Number_of_Contacts__c,(SELECT Id FROM Contacts) FROM Account WHERE Id IN :accountIds];
        for(Account a : accounts){
            List<Contact> cl = a.contacts;
            a.Number_of_Contacts__c = cl.size();
            
        }
        update accounts;
    }
}