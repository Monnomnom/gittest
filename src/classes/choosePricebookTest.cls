@isTest
public class choosePricebookTest {
    
    @isTest static void updatePricebookErrorTest(){
        
        Contact c = new Contact(FirstName= 'Finance', LastName = 'MainContact');
        insert c;
            
        BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = c.Id);
        insert ba;
        
        
        Opportunity opp = new Opportunity(Name ='TestOpportunity', StageName ='Prospecting', CloseDate = Date.today(), BusinessArea__c = ba.Id);
        upsert opp;
        
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        NewPricebooksController pricebooksController = new NewPricebooksController(sc);
        pricebooksController.opp = opp;
        pricebooksController.selectedPricebook= null;
        pricebooksController.updatePricebook2();
        boolean msgFound = false;
        
        for(Apexpages.Message msg : ApexPages.getMessages()){
            
        	if (msg.getDetail() == pricebooksController.MSG_CHOOSE_PRICEBOOK) msgFound = true;
        }
        system.assert(msgFound);
    }
    @isTest static void updatePricebookTest(){  
        
       Contact c = new Contact(FirstName= 'Finance', LastName = 'MainContact');
        insert c;
            
        BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = c.Id);
        insert ba; 
        
       Opportunity opp = new Opportunity(Name ='TestOpportunity', StageName ='Prospecting', CloseDate = Date.today(), BusinessArea__c = ba.Id);  
       Pricebook2 pb = new Pricebook2(Name ='01_Pricebook1');
       Pricebook2 pb2 = new Pricebook2(Name ='02_Pricebook2');
        
       upsert pb;
       upsert pb2;
        
       opp.Pricebook2Id = [SELECT Id 
                          	 FROM Pricebook2 
                           		WHERE Name = '02_Pricebook2'].Id;
       upsert opp; 
        
       PageReference pageRef = new PageReference('/'+opp.Id);
       Test.setCurrentPage(pageRef);
       ApexPages.StandardController sc = new ApexPages.standardController(opp);
       NewPricebooksController pricebooksController = new NewPricebooksController(sc);
       pricebooksController.opp = opp;
       upsert pricebooksController.opp;
        
       pricebooksController.selectedDivision = '01';
       pricebooksController.selectedPricebook = '01_Pricebook1';
       List<SelectOption> selDivs = pricebooksController.getDivisions();
       List<SelectOption> selPcb = pricebooksController.getPricebooks(); 
        
       //check if theres all divisions choices and pricebook choices
       System.assertEquals(5,selDivs.size());
       System.assertEquals(2,selPcb.size());
       pricebooksController.updatePricebook2();
        
       // check if Pricebook2 was changed to pricebook1 
       system.assertEquals('01_Pricebook1', [SELECT Name
                                             	FROM Pricebook2
                                             		WHERE Id =:pricebooksController.opp.Pricebook2id].Name);
        
       //check if page was redirected
       System.assertEquals(pageRef.getUrl(), ApexPages.currentPage().getUrl());

    }
}