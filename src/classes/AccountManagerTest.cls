@isTest
public class AccountManagerTest {

    @isTest static void testGetAccount(){
        Id recordId = createTestRecord();
        
        Contact objCont = new Contact();
        objCont.LastName = 'Test1';
        objCont.AccountId = recordId ;
        insert objCont ;

        
        RestRequest request = new RestRequest();
        request.requestUri = 'https://eu6.salesforce.com/services/apexrest/Accounts/' + recordId +'/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        Account thisAccount = AccountManager.getAccount();
        System.assert(thisAccount !=null);
        System.assertEquals('Test Name', thisAccount.Name);
        
    }
    
    static Id createTestRecord() {
        Account accountTest = new Account(
        Name = 'Test Name');
        insert accountTest;
        return accountTest.Id;
    }         

}