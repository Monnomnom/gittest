@isTest
public class ParkLocatorTest {
    @isTest static void testCallout(){
        
        Test.setMock(WebServiceMock.class, new ParkServiceMock());
        String s = 'Japan';
        System.assertEquals(new String[]{'Park A', 'Park B', 'Park C'}, ParkLocator.country(s));        
    }

}