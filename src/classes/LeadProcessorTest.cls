@isTest
public class LeadProcessorTest {

    @testSetup
    static void setup(){
        
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0;i<200;i++)
        {
            leads.add(new Lead(Company = 'C1', LastName='Last Name '+ i ));
        }
        insert leads;
    }
    
    static testmethod void test(){
        
        Test.startTest();
        LeadProcessor lp = new LeadProcessor();
        Id batchId = Database.executeBatch(lp);
        Test.stopTest();
        
        
        System.debug([SELECT count() FROM Lead WHERE LeadSource = 'Dreamforce']);
        System.assertEquals(200,[SELECT count() FROM Lead WHERE LeadSource = 'Dreamforce']);
    }
}