public class AddPrimaryContact implements Queueable{

    private Contact contact;
    private String abbreviation;
    
    public AddPrimaryContact(Contact contact, String abbreviation){
        this.contact = contact;
        this.abbreviation = abbreviation;
       
    }
    
    public void execute(QueueableContext context){
        
        List<Account> accounts = [SELECT Id, (SELECT Id, FirstName, LastName FROM Contacts) FROM Account WHERE BillingState =:abbreviation LIMIT 200];
        List<Contact> lc = new List<Contact>();
        For(Account a : accounts){
            Contact c = this.contact.clone(false,false,false, false);
            c.AccountId = a.Id;
            lc.add(c);
        }
        insert lc;
    }
}