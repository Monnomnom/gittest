@isTest private class assignMainContactTriggerTest {

    
    @isTest private static void checkMainContact() {
        
        List<OpportunityContactRole> ocrList = [SELECT Id, ContactId FROM OpportunityContactRole];
        Contact c = [SELECT Id FROM Contact WHERE LastName ='MainContact' LIMIT 1];
       
        for(OpportunityContactRole o: ocrList){
            
            system.AssertEquals(c.Id, o.ContactId);
        }
        
    }
    
    
    @testSetup static void createOpportunity() {
        
        Contact c = new Contact(FirstName= 'Finance', LastName = 'MainContact');
        insert c;
       
        BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = c.Id);
        insert ba; 
    	
        List<Opportunity> oppList = new List<Opportunity>();
   		
        for(Integer i = 0; i<100;i++){
        
      		Opportunity o = new Opportunity (Name ='TestOpportunity' + i , StageName ='Prospecting', CloseDate = Date.today(), BusinessArea__c = ba.Id);
        	oppList.add(o);
   		 }
  
  	    if(oppList.size() >0 ){
   			Test.startTest();
       		insert oppList;
        	Test.stopTest();
   		 }
	
	}   
}