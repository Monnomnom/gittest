@isTest
public class sendAttachmenttoContactTest {
    
    @isTest static void noContacts(){
        
        Contact c = new Contact(FirstName= 'Finance', LastName = 'MainContact');
        insert c;
            
        BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = c.Id);
        insert ba;
        
        Opportunity opp = new Opportunity(Name ='TestOpportunity', StageName ='Prospecting', CloseDate = Date.today(), BusinessArea__c = ba.Id);
   		upsert opp;
        
   		PageReference pageRef = new PageReference('https://c.eu6.visual.force.com/apex/sendAttachmenttoContact?scontrolCaching=1&'+opp.Id); 
   		ApexPages.StandardController sc = new ApexPages.standardController(opp);
        sendAttachmentController sAC = new sendAttachmentController(sc);
        sAC.opp = opp;
        Test.setCurrentPage(pageRef);
        boolean msgFound = false;
        
        for(Apexpages.Message msg : ApexPages.getMessages()){
            if ( msg.getDetail() == sAC.msg_no_contacts ){
                msgFound = true;
            }
        }
     // lol    system.assert(msgFound);        
    }
     @isTest static void noEmail(){
         
     
            
       
        
        Contact con = new Contact(FirstName ='John', LastName = 'Smith');
        upsert con;
         
        BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = con.Id);
        insert ba;
         
        Opportunity opp = new Opportunity(Name ='TestOpportunity', StageName ='Prospecting', CloseDate = Date.today(), BusinessArea__c = ba.Id);
   		upsert opp;
         
        OpportunityContactRole ocr = new OpportunityContactRole(Role='Other', ContactId = con.Id, OpportunityId = opp.Id);
        upsert ocr;
         
   		PageReference pageRef = new PageReference('https://c.eu6.visual.force.com/apex/sendAttachmenttoContact?scontrolCaching=1&'+opp.Id); 
   		ApexPages.StandardController sc = new ApexPages.standardController(opp);
        sendAttachmentController sAC = new sendAttachmentController(sc);
        sAC.opp = opp;
        Test.setCurrentPage(pageRef);
        boolean msgFound = false;
         
        for(Apexpages.Message msg : ApexPages.getMessages()){
            if (msg.getDetail() == sAC.msg_no_email){
                msgFound = true;
            }
       	}
        system.assert(msgFound);        
    }  
     @isTest static void badEmail(){
         Contact c = new Contact(FirstName= 'Finance', LastName = 'MainContact');
        insert c;
            
        BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = c.Id);
        insert ba;
         
        Opportunity opp = new Opportunity(Name ='TestOpportunity', StageName ='Prospecting', CloseDate = Date.today(), BusinessArea__c = ba.Id);
   		upsert opp;
         
        PageReference pageRef = new PageReference('https://c.eu6.visual.force.com/apex/sendAttachmenttoContact?scontrolCaching=1&'+opp.Id); 
   		ApexPages.StandardController sc = new ApexPages.standardController(opp);
        sendAttachmentController sAC = new sendAttachmentController(sc);
        sAC.opp = opp;
        Test.setCurrentPage(pageRef); 
        sAC.optional_addresses = 'bademail@bad';
        sAC.send();  
        boolean msgFound = false;
         
        for(Apexpages.Message msg : ApexPages.getMessages()){
            if (msg.getDetail() == sAC.msg_bad_email){
              msgFound = true;
            }
        }
        system.assert(msgFound);        
    } 
      @isTest static void goodEmail(){
         
            
       
          
        Contact con = new Contact(FirstName ='John', LastName = 'Smith', Email ='miuurp@gmail.com');
        upsert con;
          
        BusinessArea__c ba = new BusinessArea__c(Name ='Finance', Main_Contact__c = con.Id);
        insert ba;
        Contact con2 = new Contact(FirstName ='John2', LastName = 'Smith2', Email ='miuurp@gmail.com');
        upsert con2;
         
        Opportunity opp = new Opportunity(Name ='TestOpportunity', StageName ='Prospecting', CloseDate = Date.today(), BusinessArea__c = ba.Id);
   		upsert opp;
         
   		OpportunityContactRole ocr = new OpportunityContactRole(Role='Other', ContactId = con.Id, OpportunityId = opp.Id);
        upsert ocr;
          
   		OpportunityContactRole ocr2 = new OpportunityContactRole(Role='Other', ContactId = con2.Id, OpportunityId = opp.Id);
        upsert ocr2;
          
        PageReference pageRef = new PageReference('https://c.eu6.visual.force.com/apex/sendAttachmenttoContact?scontrolCaching=1&'+opp.Id); 
   		ApexPages.StandardController sc = new ApexPages.standardController(opp);
        sendAttachmentController sAC = new sendAttachmentController(sc);
        sAC.opp = opp;
        Test.setCurrentPage(pageRef);
        sAC.optional_addresses = 'goodemail@good.com';
        sAC.subject = 'temat';
        sAC.body = 'tresc';
          
        sAC.send();
          
        //correct addresses assign
        system.assertEquals(new List<String> {con.Email,con2.Email,sAC.optional_addresses}, sAC.email.getToAddresses());
          
        //correct subject assign  
        system.assertEquals('temat', sAC.email.getSubject());
        
        //correct body assign 
        system.assertEquals('tresc', sAC.email.getPlainTextBody()); 
          
        boolean msgFound = false;
          
        for(Apexpages.Message msg : ApexPages.getMessages()){
            if (msg.getDetail() == sAC.msg_good_email) {
                msgFound = true;
            }
        }
         //full code execution 
        system.assert(msgFound);     
       
    }
}