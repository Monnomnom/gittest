@isTest
private class assignTaskFromEmailTest {

        static testMethod void testCreateContactFrmEmail() {
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
        email.subject = 'Create Contact';
        email.plainTextBody = 'FromEmail';
        env.fromAddress = 'miuurp@gmail.com';
            
        Project__c p = new Project__c(Name = 'Inbox', Type__c ='Business') ;
        insert p;
            
        assignTaskFromEmail creatC = new assignTaskFromEmail();
        creatC.handleInboundEmail(email, env );
            
        system.AssertEquals('Create Contact', [SELECT Name FROM Task__c LIMIT 1].Name); 
        system.AssertEquals('FromEmail', [SELECT Description__c FROM Task__c LIMIT 1].Description__c);
    }
}