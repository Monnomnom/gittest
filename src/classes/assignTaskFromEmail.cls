global class assignTaskFromEmail implements Messaging.InboundEmailHandler {

  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
    Messaging.InboundEnvelope envelope) {

    	Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
    	Task__c task = new Task__c(Name=email.subject , Description__c = email.plainTextBody, Type__c ='Business');  
    	insert task;
    
    	TaskAssignment__c ta = new TaskAssignment__c(Project__c = [SELECT Id 
                                                                   FROM Project__c
                                                                   WHERE Name='Inbox'
                                                                   And OwnerId =:UserInfo.getUserId()
                                                                   LIMIT 1].Id, 
                                                 	Tasks__c = task.Id);    
   	 	insert ta;                                             

    	System.debug('====> Created task assignemnt' + ta);
    
    	result.success = true;

    	return result;

  }
   
    
}