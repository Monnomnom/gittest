public class convertProspectPageController {

    @testvisible private Account acc{get; set;}
    public Integer mcam {get; set;}  
	public Integer cam {get; set;}  
    String noMCN = Label.noMCN;
    String noCN = Label.noCN;
    String notProspect = Label.notProspect;
        
    public convertProspectPageController(ApexPages.StandardController stdController) {
		this.acc = (Account)stdController.getRecord();   
   	}
    
    public PageReference convert(){
        
        Account a = [SELECT Type,Id, Name, Main_Company_Number__c, Company_Number__c, ParentId, BillingStreet,
                     BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingLatitude, BillingLongitude
 					 FROM Account
                     WHERE Id =:acc.Id
                     LIMIT 1];
        
        if(mcam !=NULL && cam !=NULL && a.Type =='Prospect'){
  
            if([SELECT Id FROM Account WHERE Main_Company_Number__c =:mcam AND Company_Number__c =:cam AND Id !=:a.Id].size()>0) {
            	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,noCN));  
                return null;
            } else {    
                try {
                    
                    List<Account> accList = New List<Account>();
                    a.Main_Company_Number__c = mcam;
                    a.Company_Number__c =cam;
                    a.Type= 'Sales Company Data';
                    accList.add(a);
                    updateSalesCompanyData.updateData(accList);
                    PageReference pg = new PageReference('/'+acc.Id);
                    return pg;  
                }  catch ( Exception e ){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,noCN));
                        return null;
                    }

            }
        } else {
        
            if(mcam == Null){
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,noMCN));
            }
            
            if(cam == Null){
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,noCN));
            }
            
            if(a.Type != 'Prospect'){
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,notProspect));
            }
            
        return null;
        }
        
    }
}