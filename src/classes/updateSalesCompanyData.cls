public class updateSalesCompanyData {

	public static void updateData(List<Account> accList){
        
        
        List<Account> mainaccList = New List<Account>();
		List<Account> salesaccList = New List<Account>();
        
        for(Account a : accList){
            
                
            if(a.Company_Number__c != NULL && a.Main_Company_Number__c != NULL){
            
                Account mainAcc = new Account();
                
                try{
                mainAcc = [SELECT Id FROM Account WHERE Main_Company_Number__c =:a.Main_Company_Number__c AND Type = 'Main Company Data' LIMIT 1];
                system.debug('main acc already exists');    
     
                } catch(Exception e ){
                    
                   mainAcc = new Account(Main_Company_Number__c = a.Main_Company_Number__c, Name=a.Name, Type = 'Main Company Data', 
                                        BillingStreet = a.BillingStreet, BillingCity = a.BillingCity, BillingState = a.BillingState,
                                        BillingPostalCode = a.BillingPostalCode, BillingCountry = a.BillingCountry,
                                        BillingLatitude = a.BillingLatitude, BillingLongitude = a.BillingLongitude);
                   mainaccList.add(mainAcc);

                }
                
            salesaccList.add(a);    
            }
        }
        
        insert mainaccList;
        
        for(Account a:accList){
            if(a.ParentId == NULL){
                a.ParentId = [SELECT Id FROM Account WHERE Main_Company_Number__c =: a.Main_Company_Number__c AND Type = 'Main Company Data' LIMIT 1].Id;
            }
         
        }
        upsert salesaccList;
        
	}

	
}