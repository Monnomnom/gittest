@isTest
public class AccountProcessorTest {

    @isTest
    public static void accountProcessorTest(){
        
        Account a = new Account(Name = 'Account a');
       upsert a; 
        Contact c = new Contact(FirstName='Name C', LastName = 'LastName C', AccountId = a.Id);
        Contact c2 = new Contact(FirstName='Name C2', LastName = 'LastName C2', AccountId = a.Id);
         
        upsert c; upsert c2; 
        
        Set<Id> accounts = new Set<Id> {a.Id};
        
        Test.startTest();
        AccountProcessor.countContacts(accounts);
        Test.stopTest();
        
        System.debug([select Number_of_Contacts__c from Account where id = :a.id]);
        System.assertEquals(2,Integer.valueOf([select Number_of_Contacts__c from Account where id = :a.id LIMIT 1].Number_of_Contacts__c));
    }
    
}