@isTest 
private class AccountTriggerTest {

    @isTest static private  void create200Records(){
    
        List<Account> accList = [SELECT ID, Name, BillingState, ShippingState FROM Account WHERE BillingState = 'CA'];
        
        for(Account a: accList)
        {
            system.AssertEquals(a.BillingState,a.ShippingState);
        }

    }  
    
@testSetup static void createAccounts() {
    List<Account> accList = new List<Account>();
    for(Integer i = 0; i<200;i++){
        
        Account a = new Account(Name = 'Account ' + i,BillingState = 'CA' );
        accList.add(a);
    }
   System.debug(accList);
    if(accList.size() >0 ){
   
        insert accList;
    }
	
}    
}