public class ContactSearch {
public static List<Contact> searchForContacts (String last_name, String postal_code)
{
    List<Contact> lista = new List<Contact>();
    lista = [SELECT Name FROM CONTACT WHERE LastName=:last_name AND MailingPostalCode=:postal_code];
    return lista;
}
}