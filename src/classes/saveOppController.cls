public class saveOppController {
    
	@testvisible private Opportunity opp {get; set;}
	public List<OpportunityLineItem> oppLineItem { get;set;}   
	public Opportunity oppList{get;set;}
	public String ownerName{get;set;}  
	public String accountName{get;set;}
	public String pricebook{get;set;} 
	public String campaign { get;set;}
	public String lastModified { get;set;} 
	@testvisible private String msg_no_olt = 'There were no Opportunity Line Items'; 
    
	public saveOppController(ApexPages.StandardController stdController) {
    
		this.opp = (Opportunity)stdController.getRecord(); 
        
        oppLineItem = [SELECT Id, Name, Quantity, ListPrice, TotalPrice
                      	 FROM OpportunityLineItem
                      		 WHERE OpportunityId =:opp.id];  
        
        oppList = [SELECT Id, Name,Type,IsPrivate, OwnerId,LastModifiedById,LastModifiedDate, Description,CampaignId, 
                   Amount,CloseDate,AccountId,ExpectedRevenue, Pricebook2Id,StageName, Probability,NextStep,
                   LeadSource, LastActivityDate
                  	  FROM Opportunity 
                  	  	 WHERE Id =: opp.Id ]; 
        
        ownerName = [SELECT Name
                     	FROM User
                     		WHERE Id =: oppList.OwnerId].Name;
        
        try{
            accountName = [SELECT Name
                           	 FROM Account
                           		WHERE Id =: oppList.AccountId].Name;
         } catch ( Exception e ) {
            
        } 
        
        try{
            pricebook = [SELECT Name
                         	FROM Pricebook2
                         		WHERE Id =: oppList.Pricebook2Id].Name;
         } catch ( Exception e ) {
            
        } 
        
        try{
            campaign = [SELECT Name
                        	FROM Campaign
                       		   WHERE Id =: oppList.CampaignId].Name;
          } catch ( Exception e ) {
            
        } 
        
        lastModified = [SELECT Name
                        	FROM User
                        		WHERE Id =: oppList.LastModifiedbyId].Name;
        

	if(oppLineItem.size() <1 ){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,msg_no_olt));
                }    
    } 
}