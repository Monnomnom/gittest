@isTest private class convertProspectPageControllerTest {

    @TestSetup static void createProspect(){
     
        Account a = new Account(Name= 'Prospecting Acc', Type = 'Prospect');
        insert a;
        
        Account a2 = new Account(Name= 'Not Prospect', Type = 'Customer');
        insert a2; 
        
        Account mainAcc =  new Account(Name='Main Account1', Type = 'Main Company Data', Main_Company_Number__c = 2);
        insert mainAcc; 
        
        Account childAcc = new Account(Name = 'Account 6', Type = 'Sales Company Data', Main_Company_Number__c = 2, Company_Number__c = 10, BillingStreet = 'Street',
                     				   BillingCity = 'City' , BillingState = 'State' , BillingPostalCode = '00000', BillingCountry = 'Country' );
        insert childAcc;
   
    }
    
    @isTest private static void convertedProperly(){
        
        Account a = [SELECT Id,Type FROM Account WHERE Name = 'Prospecting Acc' ];
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        convertProspectPageController cppc = new convertProspectPageController(sc);
        PageReference pageRef = new PageReference('/'+a.Id);
        Test.setCurrentPage(pageRef);
        cppc.cam = 1;
        cppc.mcam = 1;
        
        cppc.convert();
        
        system.assertEquals('Sales Company Data', [SELECT Type FROM Account WHERE Company_Number__c = 1 ].Type);
    
    }
    
    @isTest private static void noNumbers(){
        
        String noMCN = Label.noMCN;
   		String noCN = Label.noCN;
        Account a = [SELECT Id,Type FROM Account WHERE Name = 'Prospecting Acc' ];
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        convertProspectPageController cppc = new convertProspectPageController(sc);
        PageReference pageRef = new PageReference('/'+a.Id);
        Test.setCurrentPage(pageRef);

        cppc.convert();
        
        boolean msgFound = false;
        
        for(Apexpages.Message msg : ApexPages.getMessages()){
            
            if (msg.getDetail() == noMCN || msg.getDetail() == noCN ) {
                msgFound = true;
            }
        }
        system.assert(msgFound);
        
    }
    
    @isTest private static void duplicates(){
        
        String noCN = Label.noCN;
        Account a = [SELECT Id,Type FROM Account WHERE Name = 'Prospecting Acc' ];
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        convertProspectPageController cppc = new convertProspectPageController(sc);
        PageReference pageRef = new PageReference('/'+a.Id);
        Test.setCurrentPage(pageRef);
        cppc.cam = 10;
        cppc.mcam = 2;
        
        cppc.convert();
        
        boolean msgFound = false;
        
        for(Apexpages.Message msg : ApexPages.getMessages()){
            
            if (msg.getDetail() == noCN ) {
                msgFound = true;
            }
        }
        system.assert(msgFound);
    }
    
    @isTest private static void notaProspect(){
        
        String notProspect = Label.notProspect;
        Account a = [SELECT Id,Type FROM Account WHERE Name = 'Not Prospect' ];
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        convertProspectPageController cppc = new convertProspectPageController(sc);
        PageReference pageRef = new PageReference('/'+a.Id);
        Test.setCurrentPage(pageRef);
        cppc.cam = 1;
        cppc.mcam = 1;
      
        cppc.convert();
        
          boolean msgFound = false;
        
        for(Apexpages.Message msg : ApexPages.getMessages()){
            
            if (msg.getDetail() == notProspect) {
                msgFound = true;
            }
        }
        system.assert(msgFound);
    }
}