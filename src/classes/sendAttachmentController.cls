public class sendAttachmentController {
    
	public Opportunity opp {get; set;}  
	public List<Contact> oppContacts { get;set;}
	@testvisible private String msg_no_contacts = 'There were no Contacts connected to this Opportunity'; 
	@testvisible private String msg_bad_email = 'Some of the email addresses are not correct.'; 
	@testvisible private String msg_good_email = 'Your file has been sent.'; 
	@testvisible private String msg_no_email = 'Some of your contacts do not have an availible email address, if you want them to receive the file, please update their data or instert the email address manually';     
    public transient Messaging.SingleEmailMessage email {get;set;}
    public String subject {get; set;}
	public String body { get; set; }
	public String optional_addresses{get;set;}
    
	public sendAttachmentController(ApexPages.StandardController stdController) {
		this.opp = (Opportunity)stdController.getRecord();

    	try{   
			oppContacts = [SELECT Email
                           	 FROM Contact
                           		WHERE Id IN (SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId =:opp.Id) ];    
			
            boolean emptyemail;
            
			for(Integer i=0;i<oppContacts.size();i++){
    			if(oppContacts.get(i).Email == null){emptyemail= true;}
            }
            
   		 	if(emptyemail){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,msg_no_email));
               }           
    }catch(Exception e) {
        if(oppContacts.size() == 0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,msg_no_contacts));
       			} 
               
          }

    }
	 public PageReference send() {
    	email = new Messaging.SingleEmailMessage(); 
    	PageReference pdf =  Page.saveOpportunityAsPDF;
     	pdf.getParameters().put('id',(String)opp.id);
   		pdf.setRedirect(true);
    	Blob b;
     	
        if(Test.isRunningTest()){
        	 b = Blob.valueOf('UNIT.TEST');
    	 }else {            
             b = pdf.getContent();
           }
         
    	Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
     	efa.setFileName('attachment.pdf');
     	efa.setBody(b);
     	String addresses;   
         
     	if(oppContacts.size() > 0) {
            if(oppContacts.get(0).Email !=null){
                addresses = oppContacts.get(0).Email;
            }
        }
         
    	for(Integer i = 1;i<oppContacts.size();i++){ 
            
        	if(!(oppContacts.get(i).Email == null)){
            	addresses += ',' + oppContacts.get(i).Email; 
        	}
     	}
    	if(optional_addresses !=null){
        	if(addresses !=null){
            	addresses += ',' + optional_addresses;
            } else {
                addresses = optional_addresses;
            }
    	 }
         
    	String[] toAddresses = addresses.split(',', 0);
         
     	for(Integer i = 0; i< toAddresses.size(); i++){
                
         		if(!Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}', toAddresses[i])){
            		 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,msg_bad_email));
        	 	}
       	 	}
        system.debug(subject);  
         
        try{
            
           	email.setSubject( subject );
      		email.setToAddresses( toAddresses );
      		email.setPlainTextBody( body );
      		email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            
      		if(!Test.isRunningTest()){
            	Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
            
            ApexPages.addmessage( new ApexPages.message(ApexPages.severity.CONFIRM, msg_good_email )); 
           
            subject = null;
            body = null;
            toAddresses = null;
            
            
     	} catch ( Exception e ) {
            System.debug(e); 
    	}
         
         return null;
         
 	}
}